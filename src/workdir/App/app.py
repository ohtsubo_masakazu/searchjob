from flask import Flask, jsonify
import neologdn
from janome.tokenizer import Tokenizer
import cloudpickle
from sklearn import model_selection, svm, metrics

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

@app.route('/predict/<title>')
def predict(title):
    token = get_token(title)
    with open("vectorizer.pkl","rb") as f:
        vectorizer = cloudpickle.loads(f.read())
    
    vec = vectorizer.transform([token]).toarray()

    with open("svm.pkl","rb") as f:
        clf = cloudpickle.loads(f.read())

    pre = clf.predict(vec)
    return jsonify({"title":title, "predict":pre[0]})

def get_token(text):
    t = Tokenizer()
    normalized_text = neologdn.normalize(text)
    tokens = t.tokenize(normalized_text) 
    word = ""
    for token in tokens:
        part_of_speech = token.part_of_speech.split(",")
        if part_of_speech[0] == "名詞" and ( part_of_speech[1] in ["一般","固有名詞"] ):
            word +=token.surface+ " "
    return word

def main():
    app.run(host="0.0.0.0",debug=True)

if __name__ == '__main__':
    main()