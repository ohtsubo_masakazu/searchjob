# 3.5 Support Vector Machine (SVM)
ようやくAIの実装に入ります。アヤメのデータとやることは同じです。

さくっと実装しましょう。

## 3.5.1 実装
### ライブラリ
* 結果の評価の為にclassification_reportを追加しています
    * 混合行列から色んな値を計算してくれます。
```python
import pandas as pd #データ読み込み
import numpy as np #データ整形
from sklearn import model_selection, svm, metrics #学習＆評価
import seaborn as sns #結果描画
from sklearn.metrics import classification_report #評価
import cloudpickle #SVMの保存
```

### データ読み込み＆整形
* stratify=labelとするとlabelに含まれるデータの比率を維持して分割してくれます
    * 今回はLikeとDislikeの数が大きく違っていて、データ数も大きくないので、このオプションを使用します
    ```python
    jobs = pd.read_json("jobs.vectors.json")
    data = jobs.iloc[:,3:]
    label = jobs.iloc[:,2]
    data_train, data_test, label_train, label_test = model_selection.train_test_split(data, label, stratify=label)
    ```

### 学習
* Dislikeのほうが圧倒的に数が多いので、Dislikeと予想すると高い確率で正解します。
* そのまま学習するとDislikeばかり予想する人工無能が出来上がります。
* そこでLikeを予想し損ねたときの罰を大きくしてLikeを予想させるようにします。
* その為のオプションがclass_weight="balanced"です
```python
clf = svm.SVC(class_weight="balanced")
clf.fit(data_train, label_train)
```

### 予測＆評価

* 結果を詳しく見るためにclassification_reportを追加しています。
* 色んな値を計算して表にしてくれます。

```python
pre = clf.predict(data_test)

cm = metrics.confusion_matrix(label_test,pre)
df_cm = pd.DataFrame(cm, columns=np.unique(label_test), index = np.unique(label_test))
df_cm.index.name = 'Actual'
df_cm.columns.name = 'Predicted'
print(df_cm)
sns.heatmap(df_cm)

print(classification_report(label_test,pre))
```

| | precision | recall | f1-score | support |
|---|---|---|---|---|
| Dislike | 0.98 | 0.99 | 0.98 | 836 |
| Like | 0.74 | 0.58 | 0.65 | 48 |
| | | | | |
| accuracy | | | 0.97 |884 |
| macro avg| 0.86 | 0.79 | 0.82 | 884 |
| weighted avg | 0.96 | 0.97 | 0.96 | 884 |



## 補足：表の読み方

必要なのは**Precision**と**Recall**
#### Precision（適合率、精度：予想のうち正解がいくつか）
* Dislikeと予想したうちで本当にDislikeだったのが98%

* Likeと予想したうちで本当にLikeだったのが74%

    **つまりこのAIで100件の仕事を提案すると74件の仕事は本当に好きな仕事**

    全ての仕事を自分で探した場合は100件のうち5件程度 （$$ \frac{48}{836+48}*100 $$）

#### Recall（再現率、検出率、感度：正解のうちいくつ予想できたか）
* 本当はDislikeのうちでDislikeと予想できたのが99%

* 本当はLikeのうちでLikeと予想出来たのが58%

  **つまり本当は100件の好きな仕事があっても58件しか提案できない** 

  今回の評価でも48件好きな仕事があるのに28件しか探し出せてない



### 保存

```python
with open("svm.pkl","wb") as f:
    f.write(cloudpickle.dumps(clf))
```



