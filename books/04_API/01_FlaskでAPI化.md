# 4.1 FlaskでAPI化

これまではJupyterの中で学習や予測をしてきました。

学習した結果を外から出来るようにAPI化していきます。

Scikitlearnをそのまま使うためにPythonのウェブアプリケーションフレームワークのFlaskを使います。

### 注意事項
Flaskで建てたサーバを直接外に公開するのは良くないようです。
[公式のドキュメント](https://flask.palletsprojects.com/en/1.1.x/deploying/#deployment)にデプロイの色んな方法がまとめてあるのでそちらを参照してください。
今回は外に公開するわけではないのでビルドインサーバを使います。

> While lightweight and easy to use, Flask’s built-in server is not suitable for production as it doesn’t scale well. Some of the options available for properly running Flask in production are documented here.



## 4.1.1 APIでやること

1. /predict/<title>で受け付ける
2. titleをtf-idfでベクトル化する
3. SVMで予測する
4. {"title":title, "predict":predict}を返す



# 4.1.2 実装

0. app.pyをリネームして避難させる。（gitで管理されているので削除してしまってもいいです

1. 03_CloudWorksで保存した"vectorizer.pkl"と"svm.pkl"をsrc/workdir/App/にコピーする

2. src/workdir/App/app.pyに下を保存する

   ```python
   from flask import Flask, jsonify
   
   app = Flask(__name__)
   app.config['JSON_AS_ASCII'] = False
   
   @app.route('/predict/<title>')
   def predict(title):
       return jsonify({"title":title})
   
   def main():
       app.run(host="0.0.0.0",debug=True)  # debug=Trueで標準出力にログを表示する
   
   if __name__ == '__main__':
       main()
   ```

3. 一旦起動してFlaskがちゃんと動くか確かめる

   ```bash
   $ docker-compose -f docker-compose.api.yml up -d
   $ curl http://localhost:5000/predict/test
   {"title":"test"}
   $ docker-compose -f docker-compose.api.yml down
   ```

   

4. src/workdir/App/app.pyにベクトル化＆予測部分を実装する

   ```python
   from flask import Flask, jsonify
   import neologdn
   from janome.tokenizer import Tokenizer
   import cloudpickle
   from sklearn import model_selection, svm, metrics
   
   app = Flask(__name__)
   app.config['JSON_AS_ASCII'] = False
   
   @app.route('/predict/<title>')
   def predict(title):
       token = get_token(title)
       with open("vectorizer.pkl","rb") as f:
           vectorizer = cloudpickle.loads(f.read())
       
       vec = vectorizer.transform([token]).toarray()
   
       with open("svm.pkl","rb") as f:
           clf = cloudpickle.loads(f.read())
   
       pre = clf.predict(vec)
       return jsonify({"title":title, "predict":pre[0]})
   
   def get_token(text):
       t = Tokenizer()
       normalized_text = neologdn.normalize(text)
       tokens = t.tokenize(normalized_text) 
       word = ""
       for token in tokens:
           part_of_speech = token.part_of_speech.split(",")
           if part_of_speech[0] == "名詞" and ( part_of_speech[1] in ["一般","固有名詞"] ):
               word +=token.surface+ " "
       return word
   
   def main():
       app.run(host="0.0.0.0")
   
   if __name__ == '__main__':
       main()
   ```

5. 再度サーバを起動して動作確認

   ```bash
   $ docker-compose -f docker-compose.api.yml up -d
   $ curl http://localhost:5000/predict/test
   {"predict":"Dislike","title":"test"}
   $ docker-compose -f docker-compose.api.yml down
   ```
   **完成！！！**

   