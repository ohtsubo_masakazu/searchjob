# 2.1 目的
* いきなりCloudWorksのデータを使うと覚えることが多すぎる
* 綺麗に御膳立てされているデータ（アヤメのデータ）を使ってScikit-learnの使い方を覚える
* データの形式
    | sepal_length | sepal_width | petal_length | petal_width | species |
    | --- | --- | --- | --- | --- |
    | がく片長 | がく片幅 | 花びら長 | 花びら幅 | 種類 |
* [sepal_length, sepal_width, petal_length, petal_width]を入力してspeciesを当てる


# 2.2 Jupyter Notebookの起動
0. /srcディレクトリに移動

1. イメージのビルド（初回のみ）
    ```
    docker-compose -f docker-compose.jupyter.yml build 
    ```
    
2. コンテナの起動
    ```
    docker-compose -f docker-compose.jupyter.yml up -d
    ```
    
3. ブラウザから http://localhost:8888/ にアクセスして下の画面が作成されれば成功
    ![image](https://i.gyazo.com/ab76902654c9972726fcb69104fefd92.png)

# 2.3 アヤメデータでScikit-learの使い方を覚える
1. ブラウザでnotebook->Iris->Iris.ipynbを開く
2. ![gif](https://i.gyazo.com/fb502a8f375e591f61ece15193375817.gif)
    1. notebookが起動されるとアイコンが緑色になります。この状態ではプログラムの状態がすべてメモリに乗ります。使い終わったらシャットダウンすることでメモリを解放することが出来ます。
    ![image](https://i.gyazo.com/6f9c8dc96e8d37ba937c1dc92ba196f0.png)
3. 上から順番にコードを実行する
    * コードを実行するには、セルを選択してShift+Enter
    * 上の部分の実行結果が下の部分に影響するので、必ず上から実行すること
    * 変数の内容が知りたいときは一番下のセルで変数名を打って実行する
        ![gif](https://i.gyazo.com/f2da623fa05b0adbaba24e8bf2f891e8.gif)



# 2.3.1 アヤメのデータ取得と確認

```
import seaborn as sns
df = sns.load_dataset("iris")
print(df)
sns.pairplot(df,hue="species")
```
* seabornは機械学習のデータをいい感じにグラフにするためのライブラリです
* 今回はそのライブラリに含まれるアヤメのデータを使います
* load_datasetで読み込んでpairplotで分布を確かめます
    * 青（setosa）は簡単に当てれそう
    * オレンジ（versicolor）と緑（virginica）は難しそう
    ![image](https://i.gyazo.com/5ea4655a09ec9a54660ee9c375a64226.png)



# 2.3.2 Scikit-Laernで使うための整形

```
from sklearn import model_selection

data = df.iloc[:,0:4]
label = df.iloc[:,-1]

data_train, data_test, label_train, label_test = model_selection.train_test_split(data, label,stratify=label)
```
* 入力をdataに入れる
    * df.iloc[:,0:4]でdfの0~3番目までの列が抽出できる
* 出力をlabelに入れる
    * df.iloc[:,-1]でdfの最後の列が抽出できる
* 学習に使うためのデータと性能を測る為のデータに分ける
    * 予測するときの性能を知るためには、学習に使っていないデータをどれだけ当てれるかが重要



# 2.3.3 Scikit-learnで学習

```
from sklearn import svm

clf = svm.SVC()
clf.fit(data_train, label_train)
clf.score(data_train,label_train)
```
* svmなのにSVC()なのはクラス分類(classification)というタスクだから
    * svmでclassificationするときにはSVC()
* 学習部分はfitだけ
* scoreでどれだけ合っているか計算



# 2.3.4 Scikit-learnで予測

```
import pandas as pd
import numpy as np
from sklearn import metrics

pre = clf.predict(data_test)

ac_score = metrics.accuracy_score(label_test,pre)
print("accuracy_score:"+str(ac_score))

cm = metrics.confusion_matrix(label_test,pre)
df_cm = pd.DataFrame(cm, columns=np.unique(label_test), index = np.unique(label_test))
df_cm.index.name = 'Actual'
df_cm.columns.name = 'Predicted'
sns.heatmap(df_cm)
df_cm
```
* pandasとnumpyはデータを扱いやすくするためのライブラリ
* sklearn.metricsの中に評価用のメソッドがたくさん用意されている
* clf.predictで予想
* 残りは結果を表示する為のコード
* accuracy_scoreは正解率
    * 97%くらい正解している予定（学習時に使う乱数によっては上下するかも



# おまけ：混合行列の読み方

![iamge](https://i.gyazo.com/5c7eb355689d1202079b7a00a3a7981f.png)
* 縦方向がActual（正解）
* 横方向がPredicted（予測）
* 例：正解はVirginicaだけど、versicolorと予測したデータが1つあった



# データの確認や結果の表示をしなければこれだけ

```
import seaborn as sns
from sklearn import model_selection
from sklearn import svm
# 読み込み
df = sns.load_dataset("iris")
# 整形
data = df.iloc[:,0:4]
label = df.iloc[:,-1]
# 分割
data_train, data_test, label_train, label_test = model_selection.train_test_split(data, label,stratify=label)
# 学習
clf = svm.SVC()
clf.fit(data_train, label_train)
# 予測
pre = clf.predict(data_test)
```